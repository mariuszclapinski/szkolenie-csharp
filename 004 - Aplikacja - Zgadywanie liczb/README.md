# Aplikacja - Zgadywanie liczb

Spróbujmy zbudować aplikację konsolową, która będzie losowała liczbę w danym zakresie, przyjmowała próbę zgadnięcia tej liczby jako input od użytkownika oraz wyświetlała informację o poprawności lub błędzie strzału.
Na początku możemy stworzyć nowy projekt aplikacji oraz dodać krótki opis, który będzie wyświetlał się jako informacja o nazwie, wersji oraz autorze.
W celu wypisania takiej informacji możemy użyć dodatkowego typu wyliczeniowego ConsoleColor Enum i zmienić kolor tekstu w konsoli np. na zielony oraz następnie zresetować ustawienie:   
#
```
string nazwaAplikacji = "Zgadywanka";
string wersjaAplikacji = "1.0.0";
string autorAplikacji = "Autor";
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("{0}: Wersja: {1} Twórca: {2}", nazwaAplikacji, wersjaAplikacji, autorAplikacji);
Console.ResetColor();
Console.ReadKey();
```

W celu ułatwienia sobie dalszej interakcji z uzytkownikiem już na samym początku możemy zbudować sobie metodę odpowiedzialną za wypisanie odpowiedniej wiadomości dla użytkownika w określonym kolorze. Dzięki metodzie nie będziemy powtarzać tego samego kodu w różnych miejscach w naszym programie.
Aby móc później użyć metody musimy ją najpierw zdefiniować zgodnie ze strukturą:
#
```
modyfikator_dostepu zwracany_typ nazwa_metody (lista_parametrów)
{
    wnętrze_metody
}
```

Struktura ta określa niezbędne elementy metod:

* modyfikator_dostępu - definiuje widoczność zmiennej lub metod;
* zwracany_typ - metoda może zwracać wartość. Typ ten określa jaki rodzaj wartości zostanie zwrócony. Jeżeli metoda nie zwraca określonej wartości, to typem zwracanym jest void;
* nazwa_metody - nazwa metody jest unikatowa oraz ważna jest wielkość liter (case sensitive). Nazwa taka nie może być identyczna z pozostałmi identyfikatorami zdefniowanymi w klasie;
* lista_parametrów - parametry umieszczone są w nawiasie. Pozwalają one na przekazywanie oraz zwracanie danych z metody. Lista parametrów danej metody odnosi się do typu, kolejności oraz liczby parametrów w metodzie. Parametry są opcjonalne, metoda nie musi ich zawierać w swojej definicji;
* wnętrze_metody - zawiera zestaw poleceń niezbednych do wykonania określonego działania.

Metoda dla naszego użytku powinna mieć następującą budowę:
#
```
static void WypiszTekst(ConsoleColor color, string message)
{
      // Zmień kolor tekstu
      Console.ForegroundColor = color;
      // Wypisz wiadomość
      Console.WriteLine(message);
       // Zresetuj kolor tekstu
      Console.ResetColor();
}
```


Następnie spróbujmy przyjąć pierwszy wpis od użytkownika i zapiszmy go w zmiennej. Niech pierwszą interakcją będzie zapytanie o imię i powitanie w grze:
#
```
Console.WriteLine("Jak masz na imię?");
string inputName = Console.ReadLine();
Console.WriteLine("Cześć {0}, zagrajmy w grę...", inputName)
```

Kolejna rzecz to inicjalizacja dwóch zmiennych niezbędnych do gry w zagadywankę tj. poprawna liczba oraz próba zgadnięcia (trafienie) użytkownika.
W celu określenia zakresu oraz wylosowania z niego liczby skorzystamy z metody Random.Next i poprosimy użytkownika o próbę zgadnięcia.:
#
```
Random random = new Random();
int poprawnaLiczba = random.Next(1, 11);
int trafienie = 0;
Console.WriteLine("Zgadnij liczbę pomiędzy 1 i 10");
```

Następnym krokiem będzie budowa pętli while, która będzie wykonywała nasz program do momentu zgadnięcia poprawnej liczby. 
Aby zapisać odpowiedź użytkownika musimy również stworzyć zmienną i dobrze byłoby zweryfikować od razu czy użytkownik stosuje się do naszych poleceń i czy wpis jest faktycznie liczbą. W tym celu niezbędne bedzie stworzenie instrukcji warunkowych weryfikujących wpis od użytkownika - jego typ oraz zakres danych. Ostatnim elementem pętli będzie umieszczenie komunikatu o błędnym trafieniu w celu obsłużenia przypadku wpisania przez użytkownika błędnej liczby z naszego zakresu:
```
while (trafienie != poprawnaLiczba)
{
	string input = Console.ReadLine();
	
	// Sprawdzenie czy napewno input to liczba
	if (!int.TryParse(input, out trafienie))
	{
		// Wypisz tekst - Error
		WypiszTekst(ConsoleColor.Red, "Proszę, wpisz liczbę");
		continue;
	}

	// Rzutowanie na Int32
	trafienie = Int32.Parse(input);

	if (trafienie < 1 || trafienie > 10 )
	{
		// Wypisz tekst - Error
		WypiszTekst(ConsoleColor.Red, "Proszę, podaj liczbę pomiędzy 1 i 10");
		continue;
	}

	// Sprawdź trafienie
	if (trafienie != poprawnaLiczba)
	{
		WypiszTekst(ConsoleColor.Red, "Błędny strzał, spróbuj ponownie");
	}
}
```

Mając zbudowaną pętlę możemy przygotować komunikat o poprawnym trafieniu oraz pytanie o ponowną grę, które również będą wywołaniem wcześniej przygotowanej metody:
#
```
WypiszTekst(ConsoleColor.Yellow, "Zgadłeś !!! Podałeś poprawną liczbę");
WypiszTekst(ConsoleColor.Blue, "Ponowna gra? \n [T - Tak, ponowna gra] \n [N - Nie, nie chcę już grać!]");
```

Przygotowując mozliwość uruchomienia naszego programu ponownie przez użytkownika nie możemy zapomnieć o zamknięciu całego bloku kodu w kolejnej pętli while wraz ze sprawdzeniem odpowiedzi od użytkownika. Ostatnia weryfikacja w instrukcji warunkowej powinna ponawiać wykonanie całej pętli programu raz jeszcze lub zwracać wartość przerywającą jej wykonanie. Wygląd aplikacji po modyfikacji:
#
```csharp
string nazwaAplikacji = "Zgadywanka";
string wersjaAplikacji = "1.0.0";
string autorAplikacji = "Autor";
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("{0}: Wersja: {1} Twórca: {2}", nazwaAplikacji, wersjaAplikacji, autorAplikacji);
Console.ResetColor();

Console.WriteLine("Jak masz na imię?");
string inputName = Console.ReadLine();
Console.WriteLine("Cześć {0}, zagrajmy w grę...", inputName);

while (true)
{
	// Obiekt random i zmienne poprawnaLiczba, trafienie
	Random random = new Random();
	int poprawnaLiczba = random.Next(1, 11);
	int trafienie = 0;
                    
	// Zapytaj o liczbę
	Console.WriteLine("Zgadnij liczbę pomiędzy 1 i 10");

	while (trafienie != poprawnaLiczba)
	{
		string input = Console.ReadLine();

		// Sprawdzenie czy napewno input to liczba
		if (!int.TryParse(input, out trafienie))
		{
			// Wypisz tekst - Error
			WypiszTekst(ConsoleColor.Red, "Proszę, wpisz liczbę");
			continue;
		}

		// Rzutowanie na Int32
		trafienie = Int32.Parse(input);

		if (trafienie < 1 || trafienie > 10 )
		{
			// Wypisz tekst - Error
			WypiszTekst(ConsoleColor.Red, "Proszę, podaj liczbę pomiędzy 1 i 10");
			continue;
		}

		// Sprawdź trafienie
		if (trafienie != poprawnaLiczba)
		{
			WypiszTekst(ConsoleColor.Red, "Błędny strzał, spróbuj ponownie");
		}
	}
	
	// Wypisz tekst - sukces
	WypiszTekst(ConsoleColor.Yellow, "Zgadłeś !!! Podałeś poprawną liczbę");

	// Zapytaj o ponowną grę
	 WypiszTekst(ConsoleColor.Blue, "Ponowna gra? \n [T - Tak, ponowna gra] \n [N - Nie, nie chcę już grać!]");

	// Pobierz odpowiedź
	string odpowiedz = Console.ReadLine().ToUpper();

	if (odpowiedz == "T")
	{
		continue;
	}
	else if (odpowiedz == "N")
	{
		return;
	}
	else
	{
		return;
	}
}	
```
