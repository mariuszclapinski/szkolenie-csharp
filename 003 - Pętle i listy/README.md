# Pętle i listy
# Pętle
Umożliwiają wielokrotne wykonanie danego bloku kodu.

## Pętla while
Pętla while wykonywana jest wielokrotnie, tak długo jak prawdziwy jest zdefiniowany warunek. 
Spróbujmy napisać programik który wypisze nam liczby od 0 do 20
```csharp
// Pętla while
int a = 0;
while(a<=20)
{
    Console.WriteLine("Liczba to: {0}", a);
    // Operator inkrementacji, wartość zostaje zwiększona o jeden
    a++;
}
Console.ReadKey();
```
## Pętla do…while
W przeciwieństwie do pętli while, która sprawdza warunek na początku, pętla do…while sprawdza warunek na końcu pętli. Czyli blok instrukcji w środku wykona się przynajmniej raz.
Wypiszmy sobie liczby od 3 do 9.

```csharp
// Pętla do...while
int i = 3;
do
{
    Console.WriteLine("Liczba to: {0}", i);
    i++;
}
while (i < 10);
```
A teraz zmieńmy warunek na i>10 i zobaczmy co się stanie. 

## Pętla For
Pętla for jest strukturą, która musi wykonać się określoną ilość razy. 
Aby pokazać wam działanie petli for musimy najpierw sobie przyswoić co to są tablice.
### Tablice 
Tablica to najprościej rzecz ujmując zestaw zaindeksowanych zmiennych tego samego typu.  Stwórzmy tablice zawierajaca dni tygodnia. W c# trzeba tablice zadeklarowac i zainicjalizowac.

deklaracja :
```csharp
string[] dniTygodnia; //typ_danych[] nazwa_tablicy; 
```
inicjalizacja:
```csharp
dniTygodnia = new string[7]; //nazwa_tablicy = new typ_danych[liczba_elementów]; 
```
przypisanie wartośći:
```csharp
    dniTygodnia[0] = "poniedziałek";
    dniTygodnia[1] = "wtorek";
    dniTygodnia[2] = "środa";
    dniTygodnia[3] = "czwartek";
    dniTygodnia[4] = "piątek";
    dniTygodnia[5] = "sobota";
    dniTygodnia[6] = "niedziela"; 
```
    
sposób zapisu w jednej linii to deklaracja tablicy i przypisanie wartości. Tutaj nie trzeba jawnie zapisywać inicjalizacji: 
```csharp
    string[] dniTygodnia = { "poniedziałek", "wtorek", "śr", "cz", "pi", "so", "ni" }; 
```

majac tablice wroćmy do petli for wypiszym sobie pierwsze 5 dni tygodnia 
```csharp
for (int indeks = 0; indeks < 5; indeks++)
{
Console.WriteLine( dniTygodnia[indeks] );
} 
```
A teraz wypiszmy wszystkie elementy naszej tablicy:
```csharp
for (int indeks = 0; indeks < dniTygodnia.Length; indeks++)
{
Console.WriteLine( dniTygodnia[indeks] );
} 
```
### Polecenie break
Dzięki poleceniu break wykonywanie pętli może zostać przerwane na żądanie. Po wypisaniu 2 dni zakończmy działanie petli.
```csharp
for (int indeks = 0; indeks < dniTygodnia.Length; indeks++)
{
    if(index=2)
    {
        break;
    }
    Console.WriteLine( dniTygodnia[indeks] );
} 
```

### Polecenie continue
Polecenie Continue przerywa dana iteracje pętli i przechodzi do następnej. Spróbujmy wypisac wszystkie dni tygodnia bez czwartku.
```csharp
for (int indeks = 0; indeks < dniTygodnia.Length; indeks++)
{
    if(index=3)
    {
        continue;
    }
    Console.WriteLine( dniTygodnia[indeks] );
} 
```
## foreach

Operując na wszystkich elementach tablicy warto skorzystać z pętli FOREACH. Daje nam ona możliwość krótszego zapisu niż pętla FOR
```csharp
foreach(string element in dniTygodnia)
{
    Console.WriteLine(element);
} 
```
Pętla foreach służy do operowania na elementach tablicy, łańcucha lub kolekcji.

###*zadanie1 łatwe*

proszę napisać program który wypisze liczby podzielne przez 3 i 5 z zakresu podanego przez użytkownika.
```csharp
int zakres;

Console.WriteLine("Podaj zakres, do ktorego chcesz odnalezc liczby");
zakres = int.Parse(Console.ReadLine());

int[] tab = new int[zakres];

for (int i = 0; i < zakres; i++)
{
    tab[i] = i;
}

for (int i = 0; i < zakres; i++)
{
    if (tab[i] % 3 == 0 && tab[i] % 5 == 0)
    Console.WriteLine(tab[i]); // program wypisuje liczby podzielne jednocześnie
                                // przez 3 i przez 5
}

Console.ReadLine();
```
*zadanie2 trudne*
Wypisac liczby pierwsze z zadanego przez użytkownika zakresu do
```csharp

int zakres, dokad, j;

Console.WriteLine("Podaj zakres, do ktorego chcesz odnalezc liczby");
zakres = int.Parse(Console.ReadLine());
            
int[] tab = new int[zakres];

for (int i = 0; i < zakres; i++)
{
    tab[i] = i;
}

dokad = (int)Math.Floor(Math.Sqrt(zakres));

//algorytm - sito eratostenesa
for (int i = 2; i <= dokad; i++)
{
    if (tab[i] != 0)
        {
            j = i + i;
            while (j < zakres)
                {
                    tab[j] = 0;
                    j += i;
                }
        }
}

//wypisz wynik
Console.WriteLine("Liczby pierwsze");
for (int i = 2; i < zakres; i++)
{
    if (tab[i] != 0)
    Console.WriteLine(i + ", ");
}
Console.ReadLine();
```
   
# Kolekcje
Kolekcje to wyspecjalizowane klasy pozwalające na przechowywanie i wyszukiwanie danych. Klasy te oferują wsparcie dla: stosów, kolejek, list. 
znajduja się w przestrzeni nazw "System.Collections".
Większość klas kolekcji implementuje te same interfejsy.
Klasy kolekcji służą do realizacji wielu celów, takich jak: dynamiczne przydzielanie pamięci dla elementów i dostęp do tych elementów na bazie indeksów.
Klasy te tworzą kolekcje obiektów klasy Object, która jest klasą bazową dla wszystkich typów danych w C#.
## Listy - Klasa ArrayList
Jest to w zasadzie alternatywa dla tablicy. 
Jednakże, w odróżnieniu do tablic, możesz dodać lub usunąć element listy na wskazanej pozycji a nasza ArrayLista zmieni rozmiar automatycznie. 
Pozwala również na dynamiczne przydzielanie pamięci, dodawanie, przeszukiwanie oraz sortowanie elementów listy. 

Stwóżmy sobie liste i dodajmy kilka liczb do niej, następnie posortujmy i wyswietlmy. 
```csharp
    ArrayList array = new ArrayList();   
            Console.WriteLine("Dodawanie numerów: ");
            array.Add(34);
            array.Add(14);
            array.Add(24);
            array.Add(34);
            array.Add(44);
     // wyswietlmy ilosc 
          Console.WriteLine("Liczba elementów: {0}", array.Count);
      // Teraz posortujemy listę
            array.Sort();
      // wyswietlmy wszystkie elementy 
    foreach (var item in array)
            {
                Console.Writeline(item);
            }
            
```
dodajmy sobie kolejny element listy ale typu string przed sortawaniem i zobaczmy co sie stanie, a nastepnie po.
```csharp
array.Add("Michał");
```
Arraylist nie ma okreslonego typu danych jaki  moze przechowywac.

usunmy 2 element listy 
```csharp
array.RemoveAt(1);
```

zadanie:
Mając  liste imion, posortujcie ja , sprawcie czy podane imie przez użytkownika znajduje sie w liscie jesli nie to komunikat brak imienia 
wyświetlcie to imie i jego pozycje na liscie. (indexof)

# typy generyczne 

Jeżeli zamierzasz umieścić w kolekcji dane tego samego typu, lepiej wykorzystać klasy oferowane przez przestrzeń nazw System.Collection.Generic.
Znajdują się tam klasy, odpowiedniki klas Stack oraz Queue (z przestrzeni System.Collection), które działają szybciej na danych tego samego typu.
Innymi słowy, typy generyczne pozwalają na napisanie klasy lub metody, która może działać z każdym typem danych, nawet własnym. 
Czyli Istnieje możliwość tworzenia własnych klas generycznych.
Nie ma w przestrzeni nazw System.Collection.Generic klasy ArrayList. 
Zamiast tego możemy jednak wykorzystać klasę List, która jest właściwie generycznym odpowiednikiem ArrayList. 
Prosty przykład użycia klasy <code>List<T></code>:     
    
## List 
```csharp
List<int> Foo = new List<int>();

Foo.Add(10);
Foo.Add(100);
Foo.Add(10000);

for (int i = 0; i < Foo.Count; i++)
    {
        Console.WriteLine("Indeks: {0} wartość: {1}", i, Foo[i]);
    }
    Console.Read();
```    
Przy deklarowaniu i utworzeniu obiektu klasy List<T> musimy podać typ danych (int), na jakim chcemy operować.
Oczywiście istnieje możliwość operowania na dowolnym typie danych — wówczas w miejsce T należy podać jego nazwę. 
*zadanie*
stworzyć liste i uzupełnic liste liczbami od 0 do 100
następnie podmienic liczby parzyste na 0 i wyświetlić liste

## Słownik
Kolejnym typem generycznym jest słownik 
public class Dictionary<TKey, TValue>
Słownik przechowuje kolekcie par kluczy/wartości. Każda wartość w miejscu “TValue” jest powiązana z danym kluczem “TKey”.

Stwórzmy sobie słownik imion i roku urodzenia  
```csharp
Dictionary<string, int> slownik = new Dictionary<string, int>();
//dodajmy elementy do słownika 
slownik.Add("mm", 1992);
slownik.Add("zz", 1993);
slownik.Add("gg", 1988);
```
Wyswietlmy po kluczu rok urodzenia gg
```csharp
int rok = slownik["gg"];
```
Usuńmy jeden element słownika 
```csharp
slownik.Remove("gg");
```
*zadanie*
Przeiterujmy po słowniku i wypiszmy klucz i wartosc 
```csharp
foreach(KeyValuePair<string, string> entry in myDictionary)
{
    // do something with entry.Value or entry.Key
}
```


# Datatable
System.Data
Reprezentuje jedną tabelę danych w pamięci.
stwórzym sobie tabele z id i imionami.
```csharp
    // Create sample Customers table, in order
    // to demonstrate the behavior of the DataTableReader.
    DataTable table = new DataTable();
  
    // Create two columns, ID and Name.
    DataColumn idColumn = table.Columns.Add("ID", typeof(int));
    table.Columns.Add("Name", typeof(string ));
  
    // Set the ID column as the primary key column.
    table.PrimaryKey = new DataColumn[] { idColumn };
    // dodajmy dane 
    table.Rows.Add(new object[] { 1, "Mary" });
    table.Rows.Add(new object[] { 2, "Andy" });
    table.Rows.Add(new object[] { 3, "Peter" });
    table.Rows.Add(new object[] { 4, "Russ" });
    
    ////wyświetlmy dane
     foreach(DataRow row in table.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    Console.WriteLine(item);
                }
            }
```

