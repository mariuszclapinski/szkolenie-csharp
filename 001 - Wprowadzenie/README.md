# Wprowadzenie

C# jest obiektowym językiem programowania stworzonym przez Microsoft. Jest to język o otwartej specyfikacji i jednocześnie jest językiem wieloplatformowym. Oprogramowanie stworzone w C# wymaga do uruchomienia odpowiedniego środowiska Common Language Runtime (CLR). W przypadku platformy Windows środowiskiem takim jest .NET Framework, istnieją również implementacje na inne systemy. Dla przykładu projekt Mono, który pozwala na uruchamianie aplikacji C# w większości dystrybucji Linuxa, BSD, OS X, Solaris a także i Windows.

Ta część szkolenia wraz z przykładowym kodem źródłowym została stworzona z wykorzystaniem dystrybucji Fedora Workstation oraz projektu Mono.

# Pierwszy projekt Hello Zakrzewo

Dla Windows z wykorzystaniem Visual Studio Community po uruchomieniu IDE należy wybrać opcję **Nowy projekt**, z listy dostępnych projektów należy wskazać Aplikacja Konsoli (.NET Framework).

Dla Linux utworzenie projektu wymaga zainstalowanego pakietu dotnet dostarczanego przez Microsoft.
Utworzenie projektu konsolowego składa się z prostej komendy:

```
dotnet new console -o HelloZakrzewo
```

Właściwie po utworzeniu projektu można przejść od razu do jego uruchomienia z domyślnie wygenerowanym kodem. W przypadku Visual Studio należy w IDE wcisnąć klawisz **F5** w przypadku Linuxa należy przejść do katalogu projektu oraz wywołać komendę `dotnet run`.

O ile w Linuxe wywołanie komendy wymaga uruchomienia z terminala i wynik działania aplikacji jest widoczny, o tyle w Windows w Visual Studio może się zdarzyć, że aplikacja po prostu po chwili zniknie.

Poczyńmy zatem małe zmiany:

- zmieńmy zapis `Hello World!` na `Hello Zakrzewo!`
- dodajmy nową linie `Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...")`
- dodajmy kolejną linię `Console.ReadKey(true)`

Wynik uruchomienia programu:
![Hello](./img/screen-001.jpg)

# Struktura aplikacji konsolowej

Spójrzmy na wygenerowany i zmieniony przez nas kod:

```CSharp
using System;

namespace HelloZakrzewo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Zakrzewo!");
            Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
            Console.ReadKey(true);
        }
    }
}
```

Można powiedzieć, że jest to kod, którego się spodziewaliśmy. Jest intuicyjny, nazwy mówią same za siebie. Ale zaczynając, od początku - a właściwie od środka.

Do wygenerowanego automatycznie kodu dopisaliśmy dwie instrukcje `Console.WriteLine(...)` oraz `Console.ReadKey()`, ich nazwy przedstawiają swoje intencje:

- WriteLine - drukuje linię na ekranie, jako parametr należy wprowadzić treść jaka ma zostać wyświetlona
- ReadKey - odczytuje wprowadzony klawisz, istnieje wersja z parametrem oraz bez parametru. W naszym przypadku wskazaliśmy parametr `true`, który tak modyfikuje metodę, że wciśnięty klawisz nie jest wyświetlany na ekranie.

Warto tutaj wspomnieć, iż obie te metody posiadają wiele wersji, jest to tak zwane przeciążanie funkcji zwane również polimorfizmem statycznym. Sama metoda `WriteLine` posiada 19 wariantów, jako parametr przyjmując różne typy zmiennych, lub nie przyjmując ich w ogóle.

Aby było jaśniej i czytelniej obie metody znajdują się w klasie statycznej `Console`, która ponowie z samej nazwy przedstawia swoje przeznaczenie. Klasa statyczna oznacza, że nie ma potrzeby tworzenia obiektu typu `Console` - ale o tym później, o klasach jak i polimorfizmie.

Idąc dalej trafiamy na kolejną warstwę `static void Main(string[] args)`. Jest to metoda, która musi istnieć w każdej aplikacji konsolowej stworzonej w C#, jest to punkt wejścia, od którego zaczyna się wykonywanie programu.

Atrybut `static` oznacza, że jest to metoda statyczna, atrybut `void` oznacza, że metoda nie zwraca żadnych danych, `Main` jest po prostu nazwą, natomiast w nawiasach zdefiniowane są parametry jakie może przyjąć metoda. W tym przypadku parametrem jest tablica zmiennych typu `string`.

Wszystko co do tej pory omówiliśmy to metody, czyli fragmenty, bloki kodu odpowiedzialne za konkretną czynność, realizujące jedno zadanie, zdefiniowane w klasach, które odzwierciedlają ich domenę.

W przypadku metody `Main`, również nie jest inaczej, znajduje się ona w klasie `Program`, mówi o tym linia `class Program`. Istota jest identyczna jak w przypadku metody `WriteLine`, która znajduje się w klasie `Console`.

Kolejny poziom `namespace` definiuje przestrzeń nazw w jakiej znajduje się klasa, w naszym przypadku klasa `Program` znajduje się w przestrzeni nazw `HelloZakrzewo`. Przestrzenie nazw w pewien sposób można traktować jak foldery, element zaszeregowania do pewnej dziedziny. Służą do organizacji typów w logiczne całości. Przestrzenie nazw pozwalają jednoznacznie odnosić się do konkretnych klas i metod, którymi jesteśmy zainteresowani, mogą to być części jakiejś biblioteki, bądź też tego samego programu.

W języku C# bardzo często korzysta się z przestrzeni nazw, nawet w tym przypadku w pierwszej linii określiliśmy, że wykorzystamy przestrzeń `System`, mówi o tym pierwsza linia `using System`. Upraszcza to zapis jednoczenie zachowując czytelność. Jednak nie zawsze warto przesadzać z upraszczaniem, ani w drugą stronę z komplikowaniem kodu. Naszą aplikację moglibyśmy zapisać inaczej, wskazując w dyrektywie `using` wprost klasę `Console`, spróbujmy:

```CSharp
using static System.Console;

namespace HelloZakrzewo
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Hello Zakrzewo!");
            WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
            ReadKey(true);
        }
    }
}
```

Moglibyśmy też w ogóle nie skorzystać z dyrektywy using:

```CSharp
namespace HelloZakrzewo
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Hello Zakrzewo!");
            System.Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
            System.Console.ReadKey(true);
        }
    }
}
```

Czytelność i jasność obu wersji kodu jest dyskusyjna, z tym, że druga wersja wypada zdecydowanie lepiej niż pierwsza.
Niemniej jest to dobry przykład na zaprezentowanie budowy i wykorzystania przestrzeni nazw - każdy element rozdzielony jest kropką. W tym przypadku w przestrzeni nazw `System`, wykorzystujemy statyczną klasę `Console`, a z niej wybieramy konkretnie `WriteLine`.

Upraszczanie zapisu może doprowadzić do sytuacji, w której metoda `WriteLine` nie będzie wskazywać jednoznacznie na konkretną przestrzeń nazw i klasę. Czy w przypadku klasy `Printer` nie może istnieć metoda `WriteLine`, która drukuje linię na kartce papieru?

Owszem może i skrócenie zapisu niekoniecznie uprości późniejszą analizę i czytelność kodu.
