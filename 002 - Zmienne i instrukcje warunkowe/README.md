# Zmienne i instrukcje warunkowe - wstęp
Zmienna to obszar w pamięci komputera oznaczony za pomocą stałej nazwy, do której możemy przypisać wartość np. tekstową, liczbową, logiczną itd. 
Każdej nowej zmiennej musimy najpierw nadać stałą nazwę i następnie możemy przypisać jej wartość. 

Opisanie nazwą konkretnej zmiennej o konkretnym typie nazywamy deklaracją zmiennej:
```
// typ unikalnaNazwa;
int zmienna;
```
Kolejną czynnością jest przypisanie zmiennej wartości, która nazywana jest inicjalizacją zmiennej:
```
zmienna = 5;
```
Deklarować i przypisywać wartość wartości możemy w jednej lini oraz zmienne możemy przypisać do innnych zmiennych.
Ważne jest jednak żeby obie zmienne mogły przechowywać ten sam typ danych:
```
int zmienna = 5;
int nowaZmienna = zmienna;
```

Tak więc zmodyfikujmy nieco nasz program "Hello Zakrzewo" i zamiast wypisywać tekst wypiszmy na ekranie zmienną 'string' z przypisaną jej wartością tekstową:

```
string zmienna = "Hello Zakrzewo!"
Console.WriteLine(zmienna);
```

# Typy danych - Typy wartościowe   

Zmienne typu wartościowego mogą mieć bezpośrednio przypisaną wartość. Dziedziczą one z klasy System.ValueType. 

Typy wartościowe zawierają dane. Niektóre z tych typów to: int, char, float, które pozwalają na składowanie numerów, liter czy liczb zmienno-przecinkowych. 
Kiedy deklarujemy wartościowy typ danych, system od razu przydziela pamięć potrzebną na przechowywanie tej zmiennej. 
W języku C# każda zmienna lub stała ma przypisany odpowiedni typ danych. Określa on jakiego rodzaju dane (wartości) będą przechowywane w danej zmiennej. 
Typ danych określa m.in.:

*Charakter danych. Czy jest to np. liczba całkowita bez znaku (typ: int) lub ciąg znaków (typ: string).
*Przestrzeń przechowywania (ile dany typ zajmuje miejsca w pamięci).
*Wartości minimalne i maksymalne jakie może reprezentować (innymi słowy zakres wartości).

Przy doborze typu zmiennej warto kierować się zasadą minimalizacji tzn. jeśli nie potrzebujemy "większego" typu to korzystamy z "mniejszego".

**byte, sbyte, short, int, long**   
Sprobujmy teraz zadeklarować kilka podstawowych zmiennych wystepujących w języku C# i odpowiedzialnych za przechowywanie wartości numerycznych:

```
byte zmiennaByte = 255;         //liczba całkowita bez znaku  (od 0 do 255)
sbyte zmiennaSbyte = -128;      //liczba całkowita ze znakiem (od -128 do 127) 
short zmiennaShort = -32768;    //liczba całkowita ze znakiem (od -32,768 do 32,767)
int zmiennaInt = 50;            //liczba całkowita ze znakiem (od -2,147,483,648 do 2,147,483,647)
long zmiennaLong = 100;         //liczba całkowita ze znakiem (od -9,223,372,036,854,775,808 do 9,223,372,036,854,775,807)
```

Przyjrzyjmy się jeszcze jednej właściwości typów: short, int, long.
Na podstawie typu int możemy pokazać możliwość zwiększenia (a w zasadzie przesunięcia) zakresu zmiennej. Powiedzmy, że do zmiennej "zmiennaInt" chcemy przypisać wartość 2500000000 (dwa i pół miliarda). Jeżeli chcielibyśmy zadeklarować naszą zmienną "zmiennaInt" o typie int to zobaczylibyśmy błąd z powodu przekroczenia zakresu, gdyż zmienne typu int mogą przyjąć wartość od -2147483648 do 2147483647. W tym przypadku należałoby użyć typu long który daje już możliwość przypisania tak dużej wartości do zmiennej. Typ long potrzebuje jednak dwa razy więcej pamięci. Jeżeli wiemy, że nasza zmienna może przyjmować tylko wartości dodatnie możemy zastosować typ int bez znaku dodając do słówka int "u" (unsigned) na początku, czyli uint. To przerzuca jakby zakres ujemny na dodatnią stronę i tym samym zwiększa (podwaja) "dodatni" zakres typu uint, co obrazuje poniższy zrzut:

```
int zmiennaInt = 2500000000;                    //błąd
uint zmiennaUint = 2500000000;                  //0 do 4,294,967,295
ulong zmiennaUlong = 18446744073709551615;      //0 do 18,446,744,073,709,551,615
ushort zmiennaUshort = 65535;                   //0 to 65,535
```

W języku C# mamy możliwość sprawdzenia ile pamięci zajmuje dana zmienna. Należy w tym wypadku użyć metody sizeof(type). Zwrócona zostanie nam wartość zajętej pamięci przez określony typ danych. Wartość ta będzie podana w bajtach. 
Oto przykład sprawdzenia rozmiaru w pamięci 32-bitowej liczby całkowitej: 
```
Console.WriteLine("Rozmiar zmiennej typu int: {0}", sizeof(int));
Console.ReadLine();
```

Wynik działania programu: Rozmiar zmiennej typu int: 4
Powyższa wartość wyrażona w bajtach -> 1 bajt to 8 bitów
Wynika zatem, że int zajmuje w pamięci 32 bity

**Float, Double, Decimal**    
Omówilismy typy danych i zmienne przechowujące liczby całkowite. W przypadku, gdy chcielibyśmy przechować w zmiennej wartości liczbowe zmiennoprzecinkowe możmey użyć następujących typów:

```
float zmiennaFloat = 1.2345F;       
double zmiennaDouble = 9.8765D;
decimal zmiennaDecimal = 1.0000M;
```

Główną różnicą jest ich precyzja. 'Decimal' mają znacznie większą precyzję i są zwykle używane w aplikacjach finansowych, które wymagają dużej dokładności. Są znacznie wolniejsze (do 20 razy w niektórych testach) niż 'double/float'.
Decimals i double/float nie mogą być również porównywane bez rzutowania, podczas gdy 'double' oraz 'float' mogą.

```
float zmiennaFloat = 1F / 3;
double zmiennaDouble = 1D / 3;
decimal zmiennaDecimal = 1M / 3;
Console.WriteLine("float: {0} double: {1} decimal: {2}", zmiennaFloat, zmiennaDouble, zmiennaDecimal);
Console.ReadLine();
```
Wynik:
```
float: 0.3333333  
double: 0.333333333333333  
decimal: 0.3333333333333333333333333333
```

**char**   
Jeśli chcielibyśmy przechować w zmiennej jeden znak to powinniśmy posłużyć się typem char. Reprezentuje on 16-bitowy znak z tablicy Unicode	(w zakresie od U +0000 do U +ffff).   
Unicode: https://unicode-table.com/pl/   
ASCII: https://www.garykessler.net/library/ascii.html   

```
char a = 'X';        // pojedynczy znak
char b = '\x0058';   // wartość heksadecymalna
char c = (char)88;   // rzutowanie
char d = '\u0058';   // Unicode

Console.WriteLine(a);
Console.ReadLine()
```

**bool**    
Słowo kluczowe bool oznacza typ zmiennej służący do przechowywania wartości logicznej: True lub False. 
W środowisku .NET Framework, odpowiednikiem tego typu jest System.Boolean.

```
bool B = true;
Console.WriteLine(B);

B = false;
Console.WriteLine(B);
```

# Typy danych - Typy referencyjne    

Typy referencyjne w odróznieniu do typów wartościowych nie przechowują rzeczywistych wartości w zmiennej, przechowują referencje do zmiennych. Innymi słowy, odnoszą się do konkretnej lokalizacji w pamięci. Przykładami wbudowanych typów referencyjnych są: obiekty (objects), typy dynamiczne (dynamics) oraz string (typ tekstowy). 

**Typ obiektowy (object type)**    
Typ obiektowy Object Type jest klasą bazową dla wszystkich typów danych w CTS (Common Type System) - jeden z bloków składowych platform .NET, który jest odpowiedzialny za opis wszystkich danych udostępnionych przez środowisko uruchomieniowe. Do typów obiektowych możemy przypisać dowolną wartość, tj. typ wartościowy, typ referencyjny oraz typ zdefiniowany przez użytkownika. Jednakże, przed przypisaniem wartości należy dokonać odpowiedniej konwersji. 
W momencie gdy typ wartościowy jest konwertowany na typ obiektowy mamy doczynienia z operacją boxing. Jeżeli obiekt jest konwertowany na typ wartościowy mamy doczyniania z operacją unboxing.

```
// Najpierw boxing - konwertowanie typu wartosciowego na obiekt
int i = 12;
object j = i;

string s = "Liczba miesięcy w roku: ";
object o = s;

// A teraz zrobimy unboxing
int liczba = (int)i;
string tekst = o.ToString();

Console.WriteLine(tekst + liczba);
Console.ReadLine();
```

**Typ dynamiczny (dynamic type)**   

W zmiennej dynamicznej możemy przechowywać dowolny typ danych. Sprawdzanie typów zmiennych odbywa się w momencie wykonywania programu. 

Składnia typów dynamicznych jest następująca - przykład:

```
dynamic zmienna = 10;
Console.WriteLine(zmienna);
Console.ReadLine();
```
Typy dynamiczne są podobne do typów obiektowych z zaznaczeniem, że sprawdzanie typów dla typów obiektowych odbywa się w trakcie kompilacji, podczas gdy sprawadzenie typów dla typów dynamicznych odbywa się w trakcie wykonywania programu. 


**Typ łańcuchowy (String type)**   

Typ łańcuchowy String Type pozwala przypisać dowolną wartość tekstową do naszej zmiennej. Łańcuchy składają się z zestawu znaków typu Char.
Wartość do stringa może zostać przypisana na jeden z dwóch sposobów: cytowany (quoted) lub @cytowany ( @quoted ).
Warto również nadmienić, że string to alias do klasy System.String, tak więc nie ma znaczenia czy deklarujemy zmienną typu string, czy System.String. Dziedziczy on z typu obiektowego Object Type. 

W naszym wcześniejszym programie "Hello Zakrzewo!" przypisaliśmy zmiennej właśnie typ łańcuchowy.
```
string zmienna = "Hello Zakrzewo!";
Console.WriteLine(zmienna);
Console.ReadLine();
```

Do poszczególnych znaków łańcucha można odwoływać się przy pomocy symboli oraz podając numer znaku (indeksu), które są numerowane od zera. Czyli pierwszy znak w ciągu ma indeks 0, kolejny - 1 itd. Spróbujmy wyświetlić w konsoli literę 'H':

```
string zmienna = "Hello Zakrzewo!";
Console.Write(zmienna[0]);
Console.ReadLine();
```

Klasa String posiada wiele wbudowanych metod, które pomagają pracować z typami łańcuchowymi. Spróbujmy wywołać parę z nich i wyświetlmy w konsoli:

```
string zmienna = "Hello Zakrzewo!";

Console.WriteLine(zmienna.ToUpper());
Console.WriteLine(zmienna.ToLower());
Console.WriteLine(zmienna.Length);
Console.WriteLine(zmienna.Replace("Hello","Siema"));

string zmienna2 = "   Hello Zakrzewo!   ";

Console.WriteLine(zmienna2.Trim());
Console.WriteLine(zmienna2.TrimStart());
Console.WriteLine(zmienna2.TrimEnd());

Console.WriteLine(String.Compare(zmienna, zmienna2) == 0);
Console.WriteLine(zmienna.Contains("Hello"));
Console.WriteLine(zmienna.Substring(6));
            
Console.ReadLine();
```

# Konwersja typów  
Konwersja typów to konwertowanie jednego typu danych na inny typ danych. Pojęcie to w języku angielskim to Type Casting.  
W języku C# istnieją dwa rodzaje konwersji:  
*konwersja niejawna (Implicit type conversion) – ten rodzaj konwersji jest dokonywany w bezpieczny sposób. Oznacza to, że dokonujemy konwersji, np. mniejszego na większy typ danych czy konwersji klasy pochodnej na klasę bazową;   

```
int wartosc1 = 0;
byte wartosc2 = 15;
// Wymuszona konwersja, dokonywana automatycznie przez kompilator
// Przypisanie wartości byte do wartości int
wartosc1 = wartosc2;
// Kolejny przykład, dodajemy dwie liczby: całkowitą i zmiennoprzecinkową
// Wynik to wartość zmiennoprzecinkowa
Console.WriteLine(34 + 34.5);
```
*konwersja jawna (Explicit type conversion) – konwersje takie wykonywane są bezpośrednio przez użytkowników za pomocą wcześniej zdefiniowanych metod. Jawne konwersje wymagają zastosowania operatora rzutowania.  
```
double liczba = 3765.47;
int i;
// Konwersja typu doble na int - użycie operatora rzutowania;
i = (int)liczba;
// Rezultat wywołania takiego programu to: 3765
Console.WriteLine(i);
Console.ReadKey();
```

Konwersja różnych typów wartościowych do łańcucha tekstowego (string):

```
int i = 32;
Console.WriteLine(i.ToString());
Console.ReadKey();
```

# Instrukcje warunkowe
Instrukcje warunkowe pozwalają programiście na określenie jednego lub więcej warunków, od których będzie zależał dalszy przebieg programu.    
**instrukcja if**   
```
int i = 10;

 if (i < 100)
{
	Console.WriteLine("Liczba jest mniejsza niż 100");
}
Console.ReadKey();

// Dobrą praktyką programistyczną w przypadku, jeżeli w bloku warunkowym mamy jedną linię kodu
// jest stosowanie skróconego zapisu instrukcji warunkowej
if (i < 50)
	Console.WriteLine("Liczba jest mniejsza niż 100");

Console.ReadKey();
```
  
**instrukcja if…else**      
Instrukcja warunkowa if…else składa się z wyrażenia logicznego oraz ewentualnego polecenia else, które zostanie wywołane, jeżeli wyrażenie logicznie jest nieprawdziwe (false).     
```
int i = 1000;

 if (i < 100)
{
	Console.WriteLine("Liczba jest mniejsza niż 100");
}
else
{
	Console.WriteLine("Liczba nie jest mniejsza niż 100");
}
Console.ReadKey();

```
   
**instrukcja if…else if…else**       
```
int i = 200;
if (i == 20)
{
    Console.WriteLine("Liczba jest równa 20");
}
else if (i == 40)
{
    Console.WriteLine("Liczba jest równa 40");
}
else if (i == 60)
{
    Console.WriteLine("Liczba jest równa 60");
}
else
{
    Console.WriteLine("Żadna z powyższych liczb nie pasuje");
}
```
   
**Instrukcja wyboru switch**        
Instrukcja warunkowa switch pozwala na sprawdzenie danej zmiennej w kontekście listy wymienionych wartości. Każda zmienna w tej instrukcji to tzw. przypadek (case). Zmienna jest sprawdzana dla każdego z przypadków w instrukcji warunkowej.   

Używając instrukcji wyboru switch należy stosować się do poniższych zasad:  
*wyrażenie użyte w instrukcji wyboru musi być typu integralnego (sbyte, byte, char, short, ushort, int, uint, long, ulong), typu wyliczeniowego lub może być klasą, która zawiera metodę pozwalającą na konwersję do typu integralnego lub wyliczeniowego;  
*można zdefiniować dowolną liczbę przypadków. Każdy przypadek składa się z wartości oraz znaku porównania (dwukropek);  
*zdefiniowany przez nasz przypadek musi być takiego samego typu jak wyrażenie (switch), musi to być stała lub literał;  
*kiedy wyrażenie będzie sprawdzane pomiędzy kolejnymi przypadkami i dojdzie do dopasowania zostanie wykonany kod wewnątrz bloku case aż do osiągnięcia instrukcji break;  
*nie każdy przypadek musi zawierać instrukcję break;  
*w instrukcji wyboru może być zastosowany opcjonalny operator przypadku domyślnego (default). Przypadek domyślny zostanie wykonany, jeżeli nie zostanie znalezione żadne dopasowanie. Instrukcja break nie jest potrzebna w przypadku domyślnym.  

```
string marka = "Audi";
switch (marka)
{
    case "BMW":
        Console.WriteLine("Wybrałeś samochód marki BMW");
        break;
    case "Porsche":
        Console.WriteLine("Wybrałeś samochód marki Porsche");
        break;
    case "Audi":
        Console.WriteLine("Wybrałeś samochód marki Audi");
        break;
    default:
        break;
}
```