using System;

namespace Pojazdy
{
    class Motocykl : AbstractPojazd
    {

        public Motocykl() : base() { }
        public Motocykl(string Marka, string Model, DateTime DataProdukcji) : base(Marka, Model, DataProdukcji) { }

        override public void Przedstaw()
        {
            Console.WriteLine($"Motocykl: {Marka} {Model} {DataProdukcji.Year}");
        }
        public void UruchomSilnik()
        {
            if (!CzyUruchomiony)
            {
                Console.WriteLine("Silnik został uruchomiony!");
                CzyUruchomiony = true;
            }
        }
        public void WylaczSilnik()
        {
            if (CzyUruchomiony)
            {
                Console.WriteLine("Silnik został wyłączony!");
                CzyUruchomiony = true;
            }
        }
    }

}