using System;

namespace Pojazdy
{
    class Hulajnoga : AbstractPojazd
    {
        public Hulajnoga() : base() { }
        public Hulajnoga(string Marka, string Model, DateTime DataProdukcji) : base(Marka, Model, DataProdukcji) { }

        override public void Przedstaw()
        {
            Console.WriteLine($"Hulajnoga: {Marka} {Model} {DataProdukcji.Year}");
        }

    }

}