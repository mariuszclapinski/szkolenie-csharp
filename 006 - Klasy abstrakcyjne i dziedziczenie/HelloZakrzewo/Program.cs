﻿using System;
using System.Collections.Generic;
using Pojazdy;

namespace HelloZakrzewo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Zakrzewo!");
            Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
            Console.ReadKey(true);

            Samochod mojePierwszeAuto = new Samochod(Marka: "Alfa Romeo",
                                                     Model: "147",
                                                     DataProdukcji: new DateTime(2001, 1, 1));


            List<AbstractPojazd> pojazdy = new List<AbstractPojazd>();
            pojazdy.Add(mojePierwszeAuto);
            pojazdy.Add(new Samochod(Marka: "Alfa Romeo",
                                       Model: "156",
                                       DataProdukcji: new DateTime(2003, 1, 1))
                         );
            pojazdy.Add(new Samochod(Marka: "Volvo",
                                       Model: "V40",
                                       DataProdukcji: new DateTime(1999, 1, 1))
                         );
            pojazdy.Add(new Samochod(Marka: "Mitsubishi",
                                       Model: "Lancer",
                                       DataProdukcji: new DateTime(2003, 1, 1))
                         );
            pojazdy.Add(new Samochod(Marka: "Opel",
                                       Model: "Astra",
                                       DataProdukcji: new DateTime(2001, 1, 1))
                         );
            pojazdy.Add(new Hulajnoga(Marka: "Lidl",
                                       Model: "SuperSpeed",
                                       DataProdukcji: new DateTime(2017, 1, 1))
                         );
            pojazdy.Add(new Hulajnoga(Marka: "Tesco",
                                       Model: "Corprider",
                                       DataProdukcji: new DateTime(2019, 1, 1))
                         );
            pojazdy.Add(new Motocykl(Marka: "Honda",
                                       Model: "CB250",
                                       DataProdukcji: new DateTime(1995, 1, 1))
                         );
            Console.WriteLine();
            Console.WriteLine("Dostępne pojazdy:");
            foreach (var pojazd in pojazdy)
            {
                pojazd.Przedstaw();
            }

        }
    }
}
