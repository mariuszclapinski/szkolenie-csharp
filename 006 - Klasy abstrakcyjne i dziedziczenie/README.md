# Klasy abstrakcyjne

Utworzyliśmy już pierwsza klasę, stworzyliśmy jej właściwości oraz metody. Jesteśmy jednak w pewien sposób ograniczeni, możemy tworzyć tylko Samochody, przypadek kiedy chcielibyśmy stworzyć element bardzo podobny, służący do przemieszczania się, bo np rower, ciężarówkę, zmusza nas do powielenia części kodu. A tego zgodnie z zasadą Don't Repeat Yourself (DRY) nie chcemy.

I tutaj pojawiają się klasy abstrakcyjne. Pewne właściwości Samochodu są powtarzalne dla innych pojazdów, należałoby więc te wspólne cechy wydzielić do odrębnej klasy abstrakcyjnej - takiej, której nie można utworzyć.

Klasy abstrakcyjne tworzymy dokładnie tak samo jak inne klasy, dodatkiem jest atrybut `abstract` przed słowem kluczowym `class`.

Wydzielmy więc części wspólne dla pojazdów do oddzielnej klasy `AbstractPojazd` utworzonej w folderze `Pojazdy`.

```CSharp
using System;

namespace Pojazdy
{
    abstract class AbstractPojazd
    {
        protected string Marka { get; set; }
        protected string Model { get; set; }
        protected DateTime DataProdukcji { get; set; }
        protected int IloscMiejsc { get; set; }
        protected int MaksymalnaPredkosc { get; set; }
        public int Przebieg { get; set; }
        public bool CzyUruchomiony { get; protected set; }

        protected AbstractPojazd(string Marka, string Model, DateTime DataProdukcji)
        {
            this.Marka = Marka;
            this.Model = Model;
            this.DataProdukcji = DataProdukcji;
        }

        protected AbstractPojazd() : this(Marka: "Noname", Model: "Noname", DataProdukcji: new DateTime())
        {

        }

        virtual public void Przedstaw()
        {
            Console.WriteLine($"Pojazd: {Marka} {Model} {DataProdukcji.Year}");
        }
    }
}
```

Pojawia się tutaj nowe słowo kluczowe `protected`. Jest to modyfikator dostępu, tak jak `public` i `private`, w przypadku `protected` właściwości poprzedzone tym modyfikatorem są chronione, tj. dostępne w ramach klasy abstrakcyjnej i w klasach dziedziczących, ale nie dostępne na zewnątrz klasy.

Ponieważ planujemy wykorzystywać te właściwości w klasie pochodnej musimy je ustawić na chronione lub publiczne.

# Dziedziczenie

Aby skorzystać z dziedziczenia, czyli przejąć pewne właściwości utworzonej klasy abstrakcyjnej zmodyfikujmy kod klasy `Samochod`. Wydzieliliśmy już pewne właściwości, kompilacja zakończy się błędem, gdyż pewnych danych już po prostu nie ma.

W C# możliwe jest dziedziczenie tylko po jednej klasie, dziedziczenie zapisuje się po znaku dwukropka zaraz po nazwie klasy:

```CSharp
using System;

namespace Pojazdy
{
    class Samochod : AbstractPojazd
    {
        public Samochod() : base() { }
        public Samochod(string Marka, string Model, DateTime DataProdukcji) : base(Marka, Model, DataProdukcji) { }

        override public void Przedstaw()
        {
            Console.WriteLine($"Samochód: {Marka} {Model} {DataProdukcji.Year}");
        }

        public void UruchomSilnik()
        {
            if (!CzyUruchomiony)
            {
                Console.WriteLine("Silnik został uruchomiony!");
                CzyUruchomiony = true;
            }
        }

        public void WylaczSilnik()
        {
            if (CzyUruchomiony)
            {
                Console.WriteLine("Silnik został wyłączony!");
                CzyUruchomiony = false;
            }
        }
    }

}
```

Do naszej klasy abstrakcyjnej przenieśliśmy metodę `Przedstaw`, pomimo, że nie ma jej w klasie `Samochod` to jest ona dostępna - i działa! Problem polega jednak na tym, że Pojazd jest mało precyzyjny, nie określa właściwie czym jest ten pojazd.

Dziedziczenie pozwala jednak na przesłanianie metod. Oznacza to, że każda klasa pochodna może implementować na swój sposób metody klasy abstrakcyjnej. Samochód może przedstawić się jako samochód, ciężarówka jako ciężarówka, hulajnoga jako hulajnoga.

W celu przesłonienia metody wykonajmy zmianę w klasie abstrakcyjnej dopisując atrybut `virtual` przed metodą `Przedstaw`.

```CSharp
virtual public void Przedstaw()
{
    Console.WriteLine($"Pojazd: {Marka} {Model} {DataProdukcji.Year}");
}
```

W klasie Samochodu stwórzmy metodę `Przedstaw` jednak poprzedźmy ją słowem `ovveride`:

```CSharp
override public void Przedstaw()
{
    Console.WriteLine($"Samochód: {Marka} {Model} {DataProdukcji.Year}");
}
```

Kompilacja i uruchomienie kodu spowoduje wyświetlenie prawidłowego przedstawienia pojazdu. Metoda klasy abstrakcyjnej została przesłonięta przez specyficzną implementację w klasie samochodu.

# Hulajnoga

W porządku, tylko jak to wszystko teraz wykorzystać? Okej, mamy kilka samochodów, ale mamy też hulajnogę, stwórzmy klasę Hulajnogi i dodajmy ją do tej samej listy co samochody - chociaż przydałby się wtedy mały refactoring - to już przecież nie tylko samochody, a pojazdy.

Zacznijmy jednak od klasy `Hulajnoga`, co by nie było, też ma przecież swoją markę, model i rok produkcji:

```CSharp
using System;

namespace Pojazdy
{
    class Hulajnoga : AbstractPojazd
    {
        public Hulajnoga() : base() { }
        public Hulajnoga(string Marka, string Model, DateTime DataProdukcji) : base(Marka, Model, DataProdukcji) { }

        override public void Przedstaw()
        {
            Console.WriteLine($"Hulajnoga: {Marka} {Model} {DataProdukcji.Year}");
        }

    }

}
```

Spróbujmy teraz dodać hulajnogę do listy samochodów:

```CSharp
samochody.Add(new Hulajnoga(Marka: "Lidl",
                            Model: "SuperSpeed",
                            DataProdukcji: new DateTime(2017, 1, 1))
                );
samochody.Add(new Hulajnoga(Marka: "Tesco",
                            Model: "Corporider",
                            DataProdukcji: new DateTime(2019, 1, 1))
```

Powyższy kod oczywiście nie zadziała, kilka wierszy wcześniej określiliśmy, że lista zawiera samochody, a nie hulajnogi. I tutaj pojawia się zasada odwrócenia zależności.

# Zasada odwrócenia zależności

Zasada odwrócenia zależności mówi, że:
`Wysokopoziomowe moduły nie powinny zależeć od modułów niskopoziomowych – zależności między nimi powinny wynikać z abstrakcji.`

Inaczej można powiedzieć, że w klasach i metodach nie powinniśmy używać nazw konkretnych klas, należy korzystać z nazw interfejsów i klas abstrakcyjnych.

Aby dodać hulajnogę do tej samej listy co samochody - po pierwsze zmieńmy nazwę listy z `samochody` na `pojazdy` a po drugie zmieńmy typ jaki ta lista przechowuje, od teraz lista powinna przechowywać obiekty pochodne dla klasy `AbstractPojazd`.

```CSharp
List<AbstractPojazd> pojazdy = new List<AbstractPojazd>();
pojazdy.Add(mojePierwszeAuto);
pojazdy.Add(new Samochod(Marka: "Alfa Romeo",
                            Model: "156",
                            DataProdukcji: new DateTime(2003, 1, 1))
                );
pojazdy.Add(new Samochod(Marka: "Volvo",
                            Model: "V40",
                            DataProdukcji: new DateTime(1999, 1, 1))
                );
pojazdy.Add(new Samochod(Marka: "Mitsubishi",
                            Model: "Lancer",
                            DataProdukcji: new DateTime(2003, 1, 1))
                );
pojazdy.Add(new Samochod(Marka: "Opel",
                            Model: "Astra",
                            DataProdukcji: new DateTime(2001, 1, 1))
                );
pojazdy.Add(new Hulajnoga(Marka: "Lidl",
                            Model: "SuperSpeed",
                            DataProdukcji: new DateTime(2017, 1, 1))
                );
pojazdy.Add(new Hulajnoga(Marka: "Tesco",
                            Model: "Corprider",
                            DataProdukcji: new DateTime(2019, 1, 1))
                );
Console.WriteLine();
Console.WriteLine("Dostępne pojazdy:");
foreach (var pojazd in pojazdy)
{
    pojazd.Przedstaw();
}
```

Właśnie w tym momencie zrealizowaliśmy polimorfizm dynamiczny, inaczej wielopostaciowość metod.

Dodajmy jeszcze może jakieś pojazdy, może motocykl? :)
