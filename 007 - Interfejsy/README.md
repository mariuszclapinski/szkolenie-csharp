# Interfejsy

Interfejs jak wskazuje nazwa jest pewnego rodzaju kontraktem, protokołem. Porównując je do sytuacji z życia, dobrym porównaniem jest np.: port USB, określa jak urządzenia mają się komunikować. Zaletą takiego protokołu jest fakt, że nie jest istotne jakie to urządzenie - o ile posługuje się odpowiednim zestawem komunikatów. I tak drukarka, myszka, nagrywarka USB komunikuje się według ustalonego standardu z innym urządzeniem i obu stronom jest bez znaczenia kim są. Identycznie jest z interfejsami w C#, określają sposób komunikowania się między klasami, jednoczenie nie dbają o to jaka to konkretnie klasa.

Gdyby wziąć klasę `AbstractPojazd` i usunąć z niej zmienne, prywatne metody i ciała metod publicznych to otrzymalibyśmy interfejs. Oczywiście musielibyśmy zmienić nazwę tego interfejsu z `AbstractPojazd` na coś bardziej odpowiedniego oraz należałoby zmienić słowo kluczowe `class` na słowo `interface`.
Wniosek jest taki, że interfejsy są bardzo podobne do klas, jednak klasą nie są, nie implementują metod, a jedynie określają jakie metody muszą się pojawić.

Istotną przewagą interfejsów nad klasami jest to, że klasa może dziedziczyć tylko po jednej klasie, lecz może implementować wiele interfejsów! W przypadku pojazdu można wykształcić interfejsy do jazdy, na który składałoby się uruchomienie silnika, zgaszenie, przyspieszenie, zwolnienie. Jednoczenie klasa pojazd może też implementować interfejs do skręcania pojazdem, albo interfejs do tankowania paliwa.

Tutaj pewna zasada, interfejsy powinny być jak najmniejsze, sprecyzowane do konkretnych zadań i nie wykraczające poza swoją dziedzinę. Jeśli stworzymy interfejs do zmiany biegów w pojeździe to nie mieszajmy go z interfejsem do skręcania pojazdem.

Spróbujmy stworzyć nowy interfejs i go zaimplementować. Załóżmy, że prowadzimy wynajem pojazdów. Każdy pojazd, albo wybrane, może zostać wynajęty. Interfejs taki powinien zawierać minimum dwie metody, jedną do wynajęcia pojazdu, drugą do zwrotu pojazdu. Stwórzmy plik `IWynajmowalny.cs`:

```CSharp
namespace Pojazdy
{
    interface IWynajmowalny
    {
        void Wynajmij();
        void Zwroc();
    }
}
```

Załóżmy, że na początku działalności wynajmujemy tylko samochody. W klasie samochodu należy wskazać jakie interfejsy ta klasa implementuje, robi się to doklądnie tak jak określenie dziedziczenia, kolejne interfejsy zapisuje się po przecinku:

```CSharp
class Samochod : AbstractPojazd, IWynajmowalny
```

Zaimplementujmy więc interfejs, kompilacja bez implementacji interfejsu zakończy się błędem.

```CSharp
public void Wynajmij()
{
    Console.WriteLine("Pojazd został wynajęty!");
}
public void Zwroc()
{
    Console.WriteLine("Pojazd został zwrócony!");
}
```

W naszym programie HelloZakrzewo posiadamy listę pojazdów, właściwie listę pojazdów

```CSharp
List<AbstractPojazd> pojazdy = new List<AbstractPojazd>();
```

Moglibyśmy więć zmienić listę aby przechowywała `IWynajmowalny`, jednakże nie będziemy wtedy mogli mieć na liście ani Motocykla ani Hulajnogi, nic co nie implementowałoby interfejsu IWynajmowalny.

Rozwiązaniem w takim momencie jest rzutowanie, próba `przerobienia` obiektu na inną klasę. I tak możemy spróbować wynająć wszystkie pojazdy wykonując rzutowanie na interfejs:

```CSharp
foreach (var pojazd in pojazdy)
{
        IWynajmowalny wynajmowalnyPojazd = (IWynajmowalny)pojazd;
        wynajmowalnyPojazd.Wynajmij();
}
```

Błędy? Skąd, program się skompiluje, problem pojawi się na etapie wykonywania. Rzutowanie na interfejs nie jest możliwe dla niektórych typów i otrzymamy błąd `System.InvalidCastException`.

Recepty na taką sytuację, są dwie:

- Możemy obsłużyć wyjątek
- Możemy sprawdzić czy obiekt implementuje interfejs
- Zastanowić się, czy nasz program jest dobrze napisany

Opcja 1:

```CSharp
try
{
    IWynajmowalny wynajmowalnyPojazd = (IWynajmowalny)pojazd;
    wynajmowalnyPojazd.Wynajmij();
}
catch (System.InvalidCastException)
{
    Console.WriteLine($"Pojazd {pojazd.ToString()} nie może zostać wynajęty!");
}
```

Opcja 2:

```CSharp
if (pojazd is IWynajmowalny)
{
    IWynajmowalny wynajmowalnyPojazd = (IWynajmowalny)pojazd;
    wynajmowalnyPojazd.Wynajmij();
}
else
{
    Console.WriteLine($"Pojazd {pojazd.ToString()} nie może zostać wynajęty!");
}
```

Opcja 3:

Do dyskusji jest czy być może lepiej byłoby implementować interfejs dla każdego pojazdu, a do samego interfejsu dodać metodę do sprawdzenia, czy auto jest do wynajęcia, lub implementować interfejs, lecz w momencie wywołania wyrzucać błąd `NotImplemented` - sytuacje mogą być różne, nie możemy wykluczyć, że kolejne pojazdy będzie dodawał inny programista, który zdecyduje nie implementować interfejsu. Być może lepiej byłoby zawrzeć to w klasie abstrakcyjnej? Ale czy każdy pojazd jest do wynajęcia? Rownież nie, musiałaby się pojawić kolejna klasa abstrakcyjna `AbstractWynajmowalnyPojazd`, z której dziedziczyłyby pojazd.
