namespace Pojazdy
{
    interface IWynajmowalny
    {
        void Wynajmij();
        void Zwroc();
    }
}