using System;

namespace Pojazdy
{
    class Samochod : AbstractPojazd, IWynajmowalny
    {
        public Samochod() : base() { }
        public Samochod(string Marka, string Model, DateTime DataProdukcji) : base(Marka, Model, DataProdukcji) { }

        override public void Przedstaw()
        {
            Console.WriteLine($"Samochód: {Marka} {Model} {DataProdukcji.Year}");
        }
        public void UruchomSilnik()
        {
            if (!CzyUruchomiony)
            {
                Console.WriteLine("Silnik został uruchomiony!");
                CzyUruchomiony = true;
            }
        }
        public void WylaczSilnik()
        {
            if (CzyUruchomiony)
            {
                Console.WriteLine("Silnik został wyłączony!");
                CzyUruchomiony = false;
            }
        }

        public void Wynajmij()
        {
            Console.WriteLine("Pojazd został wynajęty!");
        }
        public void Zwroc()
        {
            Console.WriteLine("Pojazd został zwrócony!");
        }
    }

}