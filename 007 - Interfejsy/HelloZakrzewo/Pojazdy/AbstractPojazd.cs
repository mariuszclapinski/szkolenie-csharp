using System;

namespace Pojazdy
{
    abstract class AbstractPojazd
    {
        protected string Marka { get; set; }
        protected string Model { get; set; }
        protected DateTime DataProdukcji { get; set; }
        protected int IloscMiejsc { get; set; }
        protected int MaksymalnaPredkosc { get; set; }
        public int Przebieg { get; set; }
        public bool CzyUruchomiony { get; protected set; }

        protected AbstractPojazd(string Marka, string Model, DateTime DataProdukcji)
        {
            this.Marka = Marka;
            this.Model = Model;
            this.DataProdukcji = DataProdukcji;
        }

        protected AbstractPojazd() : this(Marka: "Noname", Model: "Noname", DataProdukcji: new DateTime())
        {

        }

        virtual public void Przedstaw()
        {
            Console.WriteLine($"Pojazd: {Marka} {Model} {DataProdukcji.Year}");
        }
    }
}