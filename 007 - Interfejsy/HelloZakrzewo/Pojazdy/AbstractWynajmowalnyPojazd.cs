using System;

namespace Pojazdy
{
    abstract class AbstractWynajmowalnyPojazd : AbstractPojazd
    {
        public AbstractWynajmowalnyPojazd() : base() { }
        public AbstractWynajmowalnyPojazd(string Marka, string Model, DateTime DataProdukcji) : base(Marka, Model, DataProdukcji) { }

        virtual public void Wynajmij()
        {
            Console.WriteLine("Pojazd został wynajęty!");
        }
        virtual public void Zwroc()
        {
            Console.WriteLine("Pojazd został zwrócony!");
        }
    }
}