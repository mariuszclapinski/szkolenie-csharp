using System;

namespace Pojazdy
{
    class Samochod
    {
        string Marka { get; set; }
        string Model { get; set; }
        DateTime DataProdukcji { get; set; }
        int IloscMiejsc { get; set; }
        int MaksymalnaPredkosc { get; set; }
        public int Przebieg { get; set; }
        public bool CzyUruchomiony { get; private set; }

        public Samochod() : this(Marka: "Noname", Model: "Noname", DataProdukcji: new DateTime())
        {

        }

        public Samochod(string Marka, string Model, DateTime DataProdukcji)
        {
            this.Marka = Marka;
            this.Model = Model;
            this.DataProdukcji = DataProdukcji;
        }
        public void Przedstaw()
        {
            Console.WriteLine($"Samochód: {Marka} {Model} {DataProdukcji.Year}");
        }
        public void UruchomSilnik()
        {
            if (!CzyUruchomiony)
            {
                Console.WriteLine("Silnik został uruchomiony!");
                CzyUruchomiony = true;
            }
        }
        public void WylaczSilnik()
        {
            if (CzyUruchomiony)
            {
                Console.WriteLine("Silnik został wyłączony!");
                CzyUruchomiony = true;
            }
        }
    }

}