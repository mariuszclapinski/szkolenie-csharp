# Klasy

Rozdział ten powinien nazywać się właściwie wstępem do programowania obiektowego, z samymi klasami już mieliśmy styczność chociażby we _Wprowadzeniu_ omawiając klasę `Program`.

Programowanie obiektowe opiera się na tym aby tworzyć aplikację w sposób najbardziej zgodny z rzeczywistością i najbardziej zgodną ze sposobem postrzegania rzeczywistości przez człowieka.

Nie mówimy, że mamy czarną niegazowaną ciecz, która nadaje się do picia - mówimy, że mamy kawę. Kawa ma swoje właściwości, chociażby kolor, ilość, zawartość kofeiny, ma też swoje akcje, możemy ją zaparzyć, możemy ją wypić, możemy ją wylać, może nas poparzyć.

Idea programowania obiektowego to klasy, czyli elementy łączące właściwości (dane), oraz akcje (zachowania/metody). Klasy są definicją danych i procedur. Obiekty są instancjami tych definicji.

Załóżmy więc, że tworzymy klasę, która definiuje samochód. Dodajmy do projektu nowy folder `Pojazdy`, a w nim plik `Samochod.cs` i stwórzmy w nim klasę `Samochod`.

```CSharp
using System;

namespace Pojazdy
{
    class Samochod
    {
        string Marka { get; set; }
        string Model { get; set; }
        DateTime DataProdukcji { get; set; }
        int IloscMiejsc { get; set; }
        int MaksymalnaPredkosc { get; set; }
        int Przebieg { get; set; }

        void UruchomSilnik()
        {
            Console.WriteLine("Silnik został uruchomiony!");
        }
    }

}
```

# Obiekty

Utworzyliśmy samodzielnie naszą pierwszą klasę `Samochod`. Jak jej użyć? Powróćmy więc do naszej aplikacji konsolowej `HelloZakrzewo` i spróbujmy utworzyć obiekt klasy `Samochod`.

Czym jest właściwie obiekt? Obiekt jest instancją klasy, tworzy się go podobnie do innych typów zmiennych, tak - nasza klasa jest nowym typem! Jedyną różnicą pomiędzy zwykłym np.: `int` jest użycie operatora `new` i nazwy typu w formie metody, de facto konstruktora - ale o tym później.

```CSharp
Samochod mojePierwszeAuto = new Samochod();
```

Powyższa konstrukcja byłaby w porządku, gdyby nie fakt, ze klasa `Samochod` znajduje się w innej przestrzeni nazw, umieściliśmy ją przecież w przestrzeni `Pojazdy`. Skompilowanie kodu zakończyło by się błędem z informacją o braku odwołania do zestawu.

Poczyńmy zatem zmianę w kodzie `HelloZakrzewo` aby poprawnie utworzyć obiekt klasy `Samochod`.

```CSharp
Console.WriteLine("Hello Zakrzewo!");
Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
Console.ReadKey(true);

Pojazdy.Samochod mojePierwszeAuto = new Pojazdy.Samochod();
```

Skompilowanie nie powinno spowodować błędów. Zamiast powyższej konstrukcji możemy również użyć dyrektywy `using` i odwołać się do przestrzeni `Pojazdy`.

# Właściwości i metody obiektu

Obiekt `mojePierwszeAuto` ma przecież właściwości, jak się do nich dostać?

Dostęp do właściwości obiektu jest możliwy poprzez kropkę, tak jakby wchodzilibyśmy głębiej w ten obiekt. Ustawienie właściwości `Przebieg` obiektu typu `Samochod` ma następującą postać:

```CSharp
mojePierwszeAuto.Przebieg = 124700;
```

Spróbujmy też uruchomić auto:

```CSharp
mojePierwszeAuto.UruchomSilnik();
```

W obecnym stanie kodu nie dostaniemy jednak poprawnych podpowiedzi od IDE, nie ma dostępnej właściwości `Przebieg` ani metody `UruchomSilnik` (podpowiedzi można wywołać kombinacją klawiszy CTRL+Spacja).

![Screen 1](img/screen-001.jpg)

Wprowadzenie na siłę ustawienia właściwości spowoduje błąd z powodu swojego poziomu ochrony. Brzmi dziwnie, ale sprawa rozbija się o modyfikatory i hermetyzację...

# Modyfikatory dostępu i hermetyzacja

Jedną z idei programowania obiektowego jest hermetyzacja obiektów, polega ona na ukrywaniu pewnych właściwości lub metod. Ma to na celu między innymi ukrycie wewnętrznej organizacji obiektu.

Hermetyzacja, zwana inaczej enkapsulacją możliwa jest poprzez modyfikatory dostępu. Zajmiemy się dwoma: `public` oraz `private`.

W naszej klasie `Samochod` modyfikatory te nie zostały zdefiniowane. Brak definicji poziomu dostępu jednoznaczne jest z dostępem prywatnym, właśnie dlatego odwołując się do właściwości `Przebieg` otrzymaliśmy błąd, dostęp do właściwości jest możliwy tylko z poziomu tej klasy. Identyczna sytuacja dotyczy metody `UruchomSilnik()`.

Zmieńmy więc definicje właściwości i metody, uczyńmy ją publiczną.

```CSharp
public int Przebieg { get; set; }
[...]
public void UruchomSilnik()
{
    Console.WriteLine("Silnik został uruchomiony!");
}
```

W kodzie `HelloZakrzewo` spróbujmy wyświetlić ustawiony przebieg:

```CSharp
mojePierwszeAuto.Przebieg = 124700;
mojePierwszeAuto.UruchomSilnik();
Console.WriteLine(mojePierwszeAuto.Przebieg);
```

Kompilacja nie spowoduje błędu, a na ekranie po wciśnięciu klawisza pojawi się informacja o uruchomieniu auta i wartość właściwości `Przebieg` naszego obiektu.

Oczywiście powyższa sytuacja jest niezgodna z rzeczywistością, dlaczego sąsiad ma zmieniać przebieg mojego pierwszego auta? Skończmy z cofaniem liczników!

Spróbujmy zmodyfikować metodę UruchomSilnik w ten sposób, aby uruchomienie auta było możliwe tylko w sytuacji kiedy silnik nie jest jeszcze uruchomiony:

- dodajmy nową właściwość typu bool: CzyUruchomiony
- zmodyfikujmy metodę UruchomSilnik tak aby nie było możliwe ponowne uruchomienie silnika w przypadku kiedy jest on uruchomiony
- dodajmy metodę, która pozwala wyłączyć silnik
- zmodyfikujmy właściwość CzyUruchiomiony w taki sposób, aby było możliwe publiczne odczytanie właściwości i wyświetlmy ją na ekranie

```CSharp
using System;

namespace Pojazdy
{
    class Samochod
    {
        string Marka { get; set; }
        string Model { get; set; }
        DateTime DataProdukcji { get; set; }
        int IloscMiejsc { get; set; }
        int MaksymalnaPredkosc { get; set; }
        public int Przebieg { get; set; }
        public bool CzyUruchomiony { get; private set; }

        public void UruchomSilnik()
        {
            if (!CzyUruchomiony)
            {
                Console.WriteLine("Silnik został uruchomiony!");
                CzyUruchomiony = true;
            }
        }
        public void WylaczSilnik()
        {
            if (CzyUruchomiony)
            {
                Console.WriteLine("Silnik został wyłączony!");
                CzyUruchomiony = true;
            }
        }
    }

}
```

# Właściwości czy pola

W języku C# istnieje wyjątkowa konstrukcja właściwości, to co zostało obecnie utworzone w klasie nie licząc metod to właściwości. Symulują one zachowanie pól, jednak działają trochę inaczej, bardziej jak metody.

Wracając do kodu wyświetlającego stan silnika:

```CSharp
public bool CzyUruchomiony { get; private set; }
```

Jest to właściwość, która zawiera w sobie domyślny getter (get;) oraz setter (private set;). Różnica pomiędzy polem klasy a właściwością na pierwszy rzut oka jest nieistotna. Jednakże właściwości symulują zachowanie konstrukcji znanych z innych języków (C++, Java) gdzie przyjęło się tworzenie getterów i setterów jako odrębnych metod klasy.

```CPP
private:
    bool CzyUruchomiony;
public:
    bool getCzyUruchomiony(){ return CzyUruchomiony; }
    void setCzyUruchomiony(bool value){ CzyUruchomiony = value; }
```

Konsekwencją faktu, że właściwości są metodami jest to, że można tworzyć na nich breakpointy, oraz można je umieścić w interfejsach - o interfejsach później.

# Konstruktory

Konstruktor to wyjątkowa metoda każdej klasy, która jest wywoływała zawsze podczas tworzenia obiektu. W naszym kodzie również wykorzystaliśmy konstruktor:

```CSharp
Samochod mojePierwszeAuto = new Samochod();
```

Jest to tak zwany konstruktor domyślny, zawsze dostępny w C# - pomimo, że go nie zdefiniowaliśmy w naszej klasie. Konstruktor służy do zainicjowania obiektu, musi posiadać dostęp publiczny a jego nazwa jest identyczna jak nazwa klasy.

Stwórzmy zatem konstruktor, który ustawi domyślne właściwości `Marka` oraz `Model`.

```CSharp
public Samochod()
{
    Marka = "Noname";
    Model = "Noname";
}
```

Dodajmy też metodę `Przedstaw`, która wyświetli na ekranie markę oraz model auta, dodajmy ją do aplikacji.

```CSharp
public void Przedstaw()
{
    Console.WriteLine($"Samochód: {Marka} {Model}");
}
```

A w `HelloZakrzewo`:

```CSharp
using System;
using Pojazdy;

namespace HelloZakrzewo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Zakrzewo!");
            Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
            Console.ReadKey(true);

            Samochod mojePierwszeAuto = new Samochod();
            mojePierwszeAuto.Przedstaw();
            mojePierwszeAuto.Przebieg = 124700;
            mojePierwszeAuto.UruchomSilnik();
            Console.WriteLine(mojePierwszeAuto.Przebieg);
            Console.WriteLine($"Auto uruchomione: {mojePierwszeAuto.CzyUruchomiony}");
            mojePierwszeAuto.WylaczSilnik();
        }
    }
}
```

Wykorzystamy teraz tzw polimorfizm statyczny, czyli przeciążanie metod, utwórzmy kolejny konstruktor, który jako parametry przyjmie informacje o modelu, marce i dacie produkcji.

```CSharp
public Samochod(string Marka, string Model, DateTime DataProdukcji)
{
    this.Marka = Marka;
    this.Model = Model;
    this.DataProdukcji = DataProdukcji;
}
```

Poprawmy również kod w `HelloZakrzewo` i wykorzystajmy nowy konstruktor:

```CSharp
Samochod mojePierwszeAuto = new Samochod(Marka: "Alfa Romeo",
                                         Model: "147",
                                         DataProdukcji: new DateTime(2001, 1, 1));
```

Załóżmy, że mamy kilka aut, dodajmy kilka samochodów do listy i wylistujmy je wszystkie.

```CSharp
using System;
using System.Collections.Generic;
using Pojazdy;

namespace HelloZakrzewo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Zakrzewo!");
            Console.WriteLine("Wciśnij dowolny klawisz aby kontynuować...");
            Console.ReadKey(true);

            Samochod mojePierwszeAuto = new Samochod(Marka: "Alfa Romeo",
                                                     Model: "147",
                                                     DataProdukcji: new DateTime(2001, 1, 1));
            mojePierwszeAuto.Przedstaw();
            mojePierwszeAuto.Przebieg = 124700;
            mojePierwszeAuto.UruchomSilnik();
            Console.WriteLine(mojePierwszeAuto.Przebieg);
            Console.WriteLine($"Auto uruchomione: {mojePierwszeAuto.CzyUruchomiony}");
            mojePierwszeAuto.WylaczSilnik();


            List<Samochod> samochody = new List<Samochod>();
            samochody.Add(mojePierwszeAuto);
            samochody.Add(new Samochod(Marka: "Alfa Romeo",
                                       Model: "156",
                                       DataProdukcji: new DateTime(2003, 1, 1))
                         );
            samochody.Add(new Samochod(Marka: "Volvo",
                                       Model: "V40",
                                       DataProdukcji: new DateTime(1999, 1, 1))
                         );
            samochody.Add(new Samochod(Marka: "Mitsubishi",
                                       Model: "Lancer",
                                       DataProdukcji: new DateTime(2003, 1, 1))
                         );
            samochody.Add(new Samochod(Marka: "Opel",
                                       Model: "Astra",
                                       DataProdukcji: new DateTime(2001, 1, 1))
                         );

            Console.WriteLine();
            Console.WriteLine("Dostępne pojazdy:");
            foreach (var pojazd in samochody)
            {
                pojazd.Przedstaw();
            }
        }
    }
}
```

# Destruktory

...
